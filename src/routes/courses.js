const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../middleware/auth');
const {adminMiddleware} = require('../middleware/auth');

const CourseController = require('../app/controllers/CourseController');


router.post('/store', CourseController.store);
router.post('/cmt', CourseController.cmt);

router.get('/:id/edit', CourseController.edit);
router.put('/:id', CourseController.update);

router.get('/:slug',authMiddleware, CourseController.show);
router.delete('/:id', CourseController.destroy);
router.get('/',adminMiddleware,CourseController.create);

module.exports = router;