const newsRoute = require('./news');
const coursesRoute = require('./courses');
const loginsRoute = require('./user');
const siteRoute = require('./site');
const {authMiddleware} = require('../middleware/auth');

function route(app){
    app.use('/user', loginsRoute);
    app.use('/ho_so',authMiddleware, newsRoute);
    app.use('/courses', coursesRoute);
    app.use('/', siteRoute);
}


module.exports = route;

