
module.exports = {
  authMiddleware(req, res, next) {
    if (!req.session || !req.session.user) {
    res.send("<script>alert('Bạn Chưa Đăng nhập!');window.location.href='/user'</script>")
    } else {
    next();
    }
  },
  adminMiddleware(req, res, next) {
    if (!req.session || !req.session.user) {
      res.send("<script>alert('Bạn Chưa Đăng nhập!');window.location.href='/user'</script>")
    } else {
      if (req.session.user.role === '1') {
        next();
      } else {
        res.send("<script>alert('Bạn Không đủ Quyền Truy Cập!');window.location.href='/'</script>")
      }
    }
  }
};