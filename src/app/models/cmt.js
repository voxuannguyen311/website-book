const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Cmt = new Schema({
    slug:   { type: String },
    name:   { type: String },
    email:   { type: String },
    cmt:    { type: String },
},{
    timestamps: true,
});

module.exports = mongoose.model('Cmt', Cmt);