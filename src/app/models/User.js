const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const User = new Schema({
    email: {type: String, maxLength: 255},
    name: {type: String, maxLength: 255},
    ngay_sinh: {type: String,maxLength: 255},
    password: {type: String, maxLength: 255},
    so_dien_thoai: {type: Number, maxLength: 255},
    gioi_tinh: {type: String, maxLength: 255},
    role: {type: String}

},{
    timestamps: true,
});




module.exports = mongoose.model('User', User);