const User = require('../models/User');



class userControll {
    register(req, res){
        res.render('user/register');
    }
    
    store(req, res, next){
      const formData = req.body;
      // const hashedPassword = bcrypt.hashSync(formData.password, 10); // Hash password
      const user = new User({
        password: formData.password,
        email: formData.email,
        name: formData.name, 
        ngay_sinh: formData.ngay_sinh,
        so_dien_thoai: formData.so_dien_thoai,
        gioi_tinh: formData.gioi_tinh,
        role: formData.role
      });
      user.save();
      return res.redirect('/user');
    }

    
    login(req, res, next) {
      const userData = req.session.user;
      res.render('user/login', {
          user: userData 
      });
    }
 

  checklogin(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
  
    User.findOne({ email: email, password: password })
      .then(data => {
        if (data) {
          req.session.user = data
          return res.redirect('/');
        } else {
          res.send("<script>alert('tên hoặc mật khẩu sai!');window.location.href='/user'</script>")
        }
      })
      .catch(err => {
        res.status(500).json('Lỗi server');
      });
  }



 
}
// module.exports xuất ra ngoài exports cái gì thì khi require chúng ta nhận được cái đó
module.exports = new userControll