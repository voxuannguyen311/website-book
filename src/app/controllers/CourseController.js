const Course = require('../models/Course');
const Cmt = require("../models/cmt");

const { mongooseToObject } = require('../../utill/mongoose')
const { mutipleMongooseToObject } = require('../../utill/mongoose')

class CourseController {


    // [GET] /courses/:slug
    show(req, res, next) {   
        const slug = req.params.slug;
        const userData = req.session.user;

        Course.findOne({ slug: slug })
            .then((course) => {
                Cmt.find({ slug: slug })
                    .then((cmts) => {
                        res.render('courses/show', {
                            course: mongooseToObject(course),
                            cmt: mutipleMongooseToObject(cmts),
                            user: userData
                        });
                    })
                    .catch(next);
            })
            .catch(next);
    }

    store(req, res, next){
        const course = new Course(req.body)
        course.save()
        return res.redirect('/courses');
    }

    cmt(req, res, next) {
        const formData = req.body;
        const cmt = new Cmt({
        slug: formData.slug,
        cmt: formData.cmt,
        email: formData.email,
        name: formData.name,

        });
        cmt.save();
        const slug = req.body.slug;
        return res.redirect("/courses/" + slug);
    }


    create(req, res, next){
        const userData = req.session.user;
        Course.find({})
        .then(course => {
            res.render('courses/create', {
                course: mutipleMongooseToObject(course),
                user: userData 
            });
        })
        .catch(next);
    }

    

    edit(req, res, next){
        Course.findById(req.params.id)
        .then(course => res.render('courses/edit', {
            course: mongooseToObject(course)
        }))
        .catch(next);
    }

    update(req, res, next){
        Course.updateOne({_id: req.params.id}, req.body)
        .then(()=> res.redirect('/courses'))
        .catch(next);
    }

    destroy(req, res, next){
        Course.deleteOne({_id: req.params.id})
        .then(()=> res.redirect('back'))
        .catch(next);
    }

}







// module.exports xuất ra ngoài exports cái gì thì khi require chúng ta nhận được cái đó
module.exports = new CourseController