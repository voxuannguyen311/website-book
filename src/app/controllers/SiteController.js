const Course = require('../models/Course');

const { mutipleMongooseToObject } = require('../../utill/mongoose')

class SiteController {
    // [GET] /home
    home(req, res, next){
        // res.render('home');
        const userData = req.session.user;
        Course.find({})
            .then(courses => {
                // courses = courses.map(course => course.toObject())
                res.render('home', {
                    courses : mutipleMongooseToObject(courses),
                    user: userData 
                });
            })
            .catch(next);
    }
  
    search(req, res, next){
        const search = req.query.search;
        Course.find({ name: { $regex: ".*" + search + ".*" } })
            .then((courses) => {
                res.render('home', { courses : mutipleMongooseToObject(courses) });
            })
            .catch(next);
    }
}
// module.exports xuất ra ngoài exports cái gì thì khi require chúng ta nhận được cái đó
module.exports = new SiteController