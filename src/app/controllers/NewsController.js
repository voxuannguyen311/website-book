class NewsController {
    // [GET] /home
    index(req, res, next) {
      const userData = req.session.user;
      res.render('ho_so', { user: userData });
  }

  logout(req, res, next) {
    req.session.destroy();
  }

}
// module.exports xuất ra ngoài exports cái gì thì khi require chúng ta nhận được cái đó
module.exports = new NewsController