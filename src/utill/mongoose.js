const { Mongoose } = require("mongoose")

module.exports = {
    mutipleMongooseToObject: function(mongooses){
        if (!Array.isArray(mongooses)) return null; // kiểm tra nếu không phải là mảng
        if (typeof mongooses.map !== 'function') return null; // kiểm tra nếu không có thuộc tính map()
        return mongooses.map(Mongoose => Mongoose.toObject());
    },

    mongooseToObject: function(mongoose){
        return mongoose ? mongoose.toObject() : mongoose;
    }
};